package hotelReservation;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.IntStream;
import javax.swing.table.DefaultTableModel;
import java.util.HashMap;
import java.util.Map;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.sql.*;
/**
 * Class AddReservation - Defines the GUI and functionality for  Adding Reservation.
 * Author: Mohammad Inanloo
 */
public class AddReservation extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JComboBox<String> cityComboBox; // Use a ComboBox for city names
    private JComboBox<Integer> dayComboBox;
    private JComboBox<String> monthComboBox;
    private JComboBox<Integer> yearComboBox;
    private JTextField txtPassengerName;
    private JButton btnAddReservation;
    private List<String> cityList = new ArrayList<>(); // Define cityList here
    private JComboBox<Integer> nightsComboBox; // ComboBox for number of nights
    private JTable tableHotelRooms;
    private DefaultTableModel tableModel;
    private Map<String, String> hotelCityMap = new HashMap<>();

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    AddReservation frame = new AddReservation();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public AddReservation() {
    	setResizable(false);
    	setTitle("Add Reservation"); 
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(170, 170, 895, 500); // Adjusted the height to fit new components
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);
    
        // City ComboBox
        JLabel lblCityName = new JLabel("City Name:");
        lblCityName.setBounds(18, 11, 120, 14);
        contentPane.add(lblCityName);

        cityComboBox = new JComboBox<>();
        cityComboBox.setBounds(83, 8, 150, 20);
        populateCityComboBox(); // Populate the city names in the ComboBox
        contentPane.add(cityComboBox);

        dayComboBox = new JComboBox<>(IntStream.rangeClosed(1, 31).boxed().toArray(Integer[]::new));
        dayComboBox.setBounds(362, 430, 50, 20);
        contentPane.add(dayComboBox);

        monthComboBox = new JComboBox<>(new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"});
        monthComboBox.setBounds(377, 8, 60, 20);
        contentPane.add(monthComboBox);

        yearComboBox = new JComboBox<>(IntStream.rangeClosed(2020, 2030).boxed().toArray(Integer[]::new));
        yearComboBox.setBounds(497, 8, 60, 20);
        contentPane.add(yearComboBox);
        nightsComboBox = new JComboBox<>();
        for (int i = 1; i <= 30; i++) { // Assuming a maximum of 30 nights
            nightsComboBox.addItem(i);
        }
        nightsComboBox.setBounds(659, 380, 50, 20); // Adjust bounds as necessary
        contentPane.add(nightsComboBox);
        
        JButton btnShowHotels = new JButton("Show Hotels");
        btnShowHotels.setBounds(243, 7, 150, 23);
        btnShowHotels.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showHotelRooms();
            }
        });
        contentPane.add(btnShowHotels);
        // Add ActionListener to "Show Hotels" button
        btnShowHotels.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showHotelRooms();
            }
        });
        contentPane.add(btnShowHotels);

        txtPassengerName = new JTextField();
        txtPassengerName.setBounds(150, 380, 150, 20);
        contentPane.add(txtPassengerName);
        txtPassengerName.setColumns(10);
        txtPassengerName.setVisible(false);

        // Check-In Date Labels and ComboBoxes
        JLabel lblCheckInDate = new JLabel("Check-In Date:");
        lblCheckInDate.setBounds(310, 383, 100, 14);
        contentPane.add(lblCheckInDate);

        dayComboBox.setBounds(420, 380, 50, 20);
        monthComboBox.setBounds(480, 380, 60, 20);
        yearComboBox.setBounds(550, 380, 60, 20);

        lblCheckInDate.setVisible(false);
        dayComboBox.setVisible(false);
        monthComboBox.setVisible(false);
        yearComboBox.setVisible(false);

        // Nights ComboBox
        JLabel lblNights = new JLabel("Nights:");
        lblNights.setBounds(620, 383, 50, 14);
        contentPane.add(lblNights);
        nightsComboBox.setBounds(670, 380, 50, 20);
        lblNights.setVisible(false);
        nightsComboBox.setVisible(false);

        // Add Reservation Button
        btnAddReservation = new JButton("Add Reservation");
        btnAddReservation.setBounds(732, 379, 137, 23);
        btnAddReservation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addReservation();
            }
        });
        contentPane.add(btnAddReservation);
        btnAddReservation.setVisible(false);
        // Passenger Name Label and TextField
        JLabel lblPassengerName = new JLabel("Passenger Name:");
        lblPassengerName.setBounds(18, 383, 120, 14); // Adjust the bounds as necessary
        contentPane.add(lblPassengerName);
        lblPassengerName.setVisible(false);
        
        
        // Initialize table model and table
        tableModel = new DefaultTableModel(
        	    new String[] {
        	        "Hotel", "City", "Room Type", "View", "Capacity", 
        	        "TV", "WiFi", "Refrigerator"
        	    }, 0
        	);
        tableHotelRooms = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(tableHotelRooms);
        scrollPane.setBounds(20, 36, 849, 336);
        contentPane.add(scrollPane);
       // loadHotelData();
        tableHotelRooms.getSelectionModel().addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                boolean rowSelected = tableHotelRooms.getSelectedRow() != -1;
                lblPassengerName.setVisible(rowSelected);
                txtPassengerName.setVisible(rowSelected);
                lblCheckInDate.setVisible(rowSelected);
                dayComboBox.setVisible(rowSelected);
                monthComboBox.setVisible(rowSelected);
                yearComboBox.setVisible(rowSelected);
                lblNights.setVisible(rowSelected);
                nightsComboBox.setVisible(rowSelected);
                btnAddReservation.setVisible(rowSelected);
            }
        });
        loadHotelData();
    }

    // Method to load hotel data from hotels.csv
    private void loadHotelData() {
        try (BufferedReader br = new BufferedReader(new FileReader("hotels.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                if (data.length >= 3) { // Check if the line has at least 3 elements
                    String hotelName = data[0].trim();
                    String city = data[2].trim(); // City is the third element
                    hotelCityMap.put(hotelName, city);
                    System.out.println("Loaded hotel: " + hotelName + " in city: " + city); // Debugging print
                }
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Error loading hotels file.", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    private void showHotelRooms() {
        String selectedCity = (String) cityComboBox.getSelectedItem();
        tableModel.setRowCount(0);

        String query = "SELECT rooms.*, hotels.name AS hotel_name, cities.name AS city_name FROM rooms "
                     + "JOIN hotels ON rooms.hotel_id = hotels.id "
                     + "JOIN cities ON hotels.city_id = cities.id "
                     + "WHERE cities.name = ?";

        try (Connection conn = DatabaseUtil.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setString(1, selectedCity);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String hotelName = rs.getString("hotel_name");
                String roomType = rs.getString("room_type");
                String capacity = rs.getString("capacity");
                String view = rs.getString("view");
                String tv = rs.getBoolean("tv") ? "Yes" : "No";
                String wifi = rs.getBoolean("wifi") ? "Yes" : "No";
                String refrigerator = rs.getBoolean("refrigerator") ? "Yes" : "No";
                tableModel.addRow(new Object[]{hotelName, selectedCity, roomType, view, capacity, tv, wifi, refrigerator});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, "Error reading rooms from database.", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    private void addReservation() {
        String passengerName = txtPassengerName.getText();
        int selectedRow = tableHotelRooms.getSelectedRow();

        if (selectedRow == -1) {
            JOptionPane.showMessageDialog(this, "No hotel room selected.", "Selection Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        String hotelName = tableHotelRooms.getValueAt(selectedRow, 0).toString();
        String city = tableHotelRooms.getValueAt(selectedRow, 1).toString();
        String roomType = tableHotelRooms.getValueAt(selectedRow, 2).toString();
        String roomView = tableHotelRooms.getValueAt(selectedRow, 3).toString();
        String checkInDate = dayComboBox.getSelectedItem() + "-" + monthComboBox.getSelectedItem() + "-" + yearComboBox.getSelectedItem();
        int nights = (Integer) nightsComboBox.getSelectedItem();

        String query = "INSERT INTO reservations (passenger_name, hotel_id, room_type, view, check_in_date, nights) "
                     + "VALUES (?, (SELECT id FROM hotels WHERE name = ?), ?, ?, ?, ?)";

        try (Connection conn = DatabaseUtil.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setString(1, passengerName);
            stmt.setString(2, hotelName);
            stmt.setString(3, roomType);
            stmt.setString(4, roomView);
            stmt.setDate(5, java.sql.Date.valueOf(LocalDate.parse(checkInDate, DateTimeFormatter.ofPattern("d-MMM-yyyy"))));
            stmt.setInt(6, nights);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(this, "Reservation added successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error saving reservation.");
            ex.printStackTrace();
        }
    }

    private void populateCityComboBox() {
        try (Connection conn = DatabaseUtil.getConnection();
             PreparedStatement stmt = conn.prepareStatement("SELECT name FROM cities");
             ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                cityComboBox.addItem(rs.getString("name"));
                cityList.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
