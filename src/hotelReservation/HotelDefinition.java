
package hotelReservation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.*;
import java.sql.*;
/**
 * Class HotelDefinition - Defines the GUI and functionality for defining hotels.
 * Author: Mohammad Inanloo
 */
public class HotelDefinition extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtHotelName;
    private JComboBox<Integer> cmbHotelStar;
    private JComboBox<String> cmbHotelCity;
    private JButton btnSave;
    private JButton btnRoomDefinition;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                HotelDefinition frame = new HotelDefinition();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the frame for Hotel Definition.
     */
    public HotelDefinition() {
        setResizable(false);
        setTitle("Hotel Definition");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(150, 150, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        initializeComponents();
        populateCities();
    }

    /**
     * Initialize GUI components.
     */
    private void initializeComponents() {
        // Hotel Name Label and Text Field
        JLabel lblHotelName = new JLabel("Hotel Name:");
        lblHotelName.setBounds(10, 11, 86, 14);
        contentPane.add(lblHotelName);

        txtHotelName = new JTextField();
        txtHotelName.setBounds(106, 8, 318, 20);
        contentPane.add(txtHotelName);
        txtHotelName.setColumns(10);

        // Star Rating Label and Combo Box
        JLabel lblHotelStar = new JLabel("Star Rating:");
        lblHotelStar.setBounds(10, 36, 86, 14);
        contentPane.add(lblHotelStar);

        cmbHotelStar = new JComboBox<>(new Integer[]{1, 2, 3, 4, 5});
        cmbHotelStar.setBounds(106, 33, 50, 20);
        contentPane.add(cmbHotelStar);

        // Hotel City Label and Combo Box
        JLabel lblHotelCity = new JLabel("City:");
        lblHotelCity.setBounds(10, 61, 86, 14);
        contentPane.add(lblHotelCity);

        cmbHotelCity = new JComboBox<>();
        cmbHotelCity.setBounds(106, 58, 318, 20);
        contentPane.add(cmbHotelCity);

        // Save Button
        btnSave = new JButton("Save Hotel");
        btnSave.addActionListener(e -> saveHotelData());
        btnSave.setBounds(10, 86, 117, 23);
        contentPane.add(btnSave);

        // Room Definition Button
        btnRoomDefinition = new JButton("Define Rooms");
        btnRoomDefinition.addActionListener(e -> openRoomDefinition());
        btnRoomDefinition.setBounds(137, 86, 117, 23);
        contentPane.add(btnRoomDefinition);
    }

    /**
     * Populate cities in the Combo Box from a file.
     */
    private void populateCities() {
        String query = "SELECT name FROM cities";
        try (Connection conn = DatabaseUtil.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query);
             ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                cmbHotelCity.addItem(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Save hotel data to a file.
     */
    private void saveHotelData() {
        String hotelName = txtHotelName.getText().trim();
        int starRating = (Integer) cmbHotelStar.getSelectedItem();
        String city = (String) cmbHotelCity.getSelectedItem();

        String query = "INSERT INTO hotels (name, stars, city_id) VALUES (?, ?, (SELECT id FROM cities WHERE name = ?))";
        try (Connection conn = DatabaseUtil.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setString(1, hotelName);
            stmt.setInt(2, starRating);
            stmt.setString(3, city);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(this, "Hotel data saved successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error saving hotel data.");
            ex.printStackTrace();
        }
    }

    /**
     * Open the Room Definition frame.
     */
    private void openRoomDefinition() {
        HotelRoomDefinition roomDefinitionFrame = new HotelRoomDefinition();
        roomDefinitionFrame.setVisible(true);
    }
}
