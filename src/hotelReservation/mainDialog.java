package hotelReservation;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.*;

/**
 * Main dialog of the Simple Hotel Reservation application.
 * Author: Mohammad Inanloo
 */
/*
 * Code Explanation
    Class Definition and Main Method:
    • The mainDialog class extends JFrame and includes a main method that initiates the event dispatch thread, leading to the creation of the main dialog frame.
      Constructor - mainDialog():
    • The constructor sets basic frame properties like title, size, and default close operation.
    • It calls initializeLabels(), loadCounts(), createButtons(), and addImageAndAuthorInfo() methods to set up the JFrame's components.
      Initializing Labels - initializeLabels():
    • This method initializes and sets properties for labels lblHotelCount, lblRoomCount, and lblFutureReservations.
    • These labels display information about the number of hotels, rooms, and future reservations, respectively.
      Loading Counts - loadCounts():
    • Calls loadHotelCount(), loadRoomCount(), and loadFutureReservationsCount() to fetch and display the current count data from the MySQL database.
      Creating Buttons - createButtons():
    • This method creates buttons for navigating to other JFrames (HotelDefinition, HotelRoomDefinition, AddReservation, ListReservation).
    • Each button is associated with an action listener that opens the corresponding JFrame.
      Helper Methods:
    • createButton(String text, int x, int y) creates a JButton with specified text and position.
    • openFrame(JFrame frame) sets the default close operation for the newly opened frame and makes it visible.
    • addImageAndAuthorInfo() adds an author section with image and text labels.
      Loading Data from Database:
    • loadHotelCount(), loadRoomCount(), and loadFutureReservationsCount() read data from the MySQL database and update the counts on the labels.
    Each method and function is carefully designed to ensure a seamless and efficient user experience, making the mainDialog an integral part of the Hotel Reservation System.
 */

public class mainDialog extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JLabel lblHotelCount;
    private JLabel lblRoomCount;
    private JLabel lblFutureReservations;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                mainDialog frame = new mainDialog();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the frame.
     */
    public mainDialog() {
        setResizable(false);
        setTitle("Simple Hotel Reservation");  

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 453, 333);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/icon.png")));

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        initializeLabels();
        loadCounts();

        createButtons();
        addImageAndAuthorInfo();
    }
    private void loadCounts() {
        loadHotelCount();
        loadRoomCount();
        loadFutureReservationsCount();
    }
    private void initializeLabels() {
        lblHotelCount = new JLabel("Hotels: Loading...");
        lblHotelCount.setFont(new Font("Tahoma", Font.PLAIN, 14));
        lblHotelCount.setBounds(216, 20, 152, 14);

        lblRoomCount = new JLabel("Rooms: Loading...");
        lblRoomCount.setFont(new Font("Tahoma", Font.PLAIN, 14));
        lblRoomCount.setBounds(216, 44, 200, 14);

        lblFutureReservations = new JLabel("Future Reservations: Loading...");
        lblFutureReservations.setFont(new Font("Tahoma", Font.PLAIN, 14));
        lblFutureReservations.setBounds(216, 68, 250, 23);

        contentPane.add(lblHotelCount);
        contentPane.add(lblRoomCount);
        contentPane.add(lblFutureReservations);
    }

    private void loadHotelCount() {
        int hotelCount = 0;
        String query = "SELECT COUNT(*) FROM hotels";
        try (Connection conn = DatabaseUtil.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query);
             ResultSet rs = stmt.executeQuery()) {
            if (rs.next()) {
                hotelCount = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        lblHotelCount.setText("Hotels: " + hotelCount);
    }

    private void createButtons() {
        JButton btnHotelDefinition = createButton("Hotel Definition", 216, 112);
        btnHotelDefinition.addActionListener(e -> openFrame(new HotelDefinition()));

        JButton btnRoomDefinition = createButton("Hotel Room Definition", 216, 145);
        btnRoomDefinition.addActionListener(e -> openFrame(new HotelRoomDefinition()));

        JButton btnAddReservation = createButton("Book from list", 216, 178);
        btnAddReservation.addActionListener(e -> openFrame(new AddReservation()));

        JButton btnListReservation = createButton("List Reservation", 216, 211);
        btnListReservation.addActionListener(e -> openFrame(new ListReservation()));
    }

    private JButton createButton(String text, int x, int y) {
        JButton button = new JButton(text);
        button.setBounds(x, y, 181, 23);
        contentPane.add(button);
        return button;
    }

    private void openFrame(JFrame frame) {
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }

    private void addImageAndAuthorInfo() {
        JLabel lblImage = new JLabel("");
        lblImage.setIcon(new ImageIcon(mainDialog.class.getResource("/resources/uni-164X164.png")));
        lblImage.setBounds(21, 10, 164, 168);
        contentPane.add(lblImage);

        JLabel lblAuthor = new JLabel("Mohammad Inanloo");
        lblAuthor.setFont(new Font("Tahoma", Font.PLAIN, 12));
        lblAuthor.setBounds(32, 188, 122, 46);
        contentPane.add(lblAuthor);

        JLabel lblStudentId = new JLabel("B2210.033130");
        lblStudentId.setFont(new Font("Tahoma", Font.PLAIN, 12));
        lblStudentId.setBounds(32, 214, 122, 23);
        contentPane.add(lblStudentId);
    }
    private void loadFutureReservationsCount() {
        int futureReservations = 0;
        LocalDate today = LocalDate.now();
        String query = "SELECT COUNT(*) FROM reservations WHERE check_in_date > ?";
        try (Connection conn = DatabaseUtil.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setDate(1, java.sql.Date.valueOf(today));
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                futureReservations = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        lblFutureReservations.setText("Future Reservations: " + futureReservations);
    }


	private void loadRoomCount() {
	    int roomCount = 0;
	    String query = "SELECT COUNT(*) FROM rooms";
	    try (Connection conn = DatabaseUtil.getConnection();
	         PreparedStatement stmt = conn.prepareStatement(query);
	         ResultSet rs = stmt.executeQuery()) {
	        if (rs.next()) {
	            roomCount = rs.getInt(1);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    }
	    lblRoomCount.setText("Rooms: " + roomCount);
	}

//    private void loadFutureReservationsCount() {
//        int futureReservations = 0;
//        LocalDate today = LocalDate.now();
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MMM-yyyy");
//
//        try (BufferedReader br = new BufferedReader(new FileReader("reservations.csv"))) {
//            String line;
//            while ((line = br.readLine()) != null) {
//                String[] data = line.split(",");
//                if (data.length > 5) {
//                    LocalDate checkInDate = LocalDate.parse(data[5], formatter);
//                    if (checkInDate.isAfter(today)) {
//                        futureReservations++;
//                    }
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        lblFutureReservations.setText("Future Reservations: " + futureReservations);
//    }
}
