package hotelReservation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.*;

/**
 * Class HotelRoomDefinition - Defines the GUI and functionality for defining hotel rooms.
 * Author: Mohammad Inanloo
 */
public class HotelRoomDefinition extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtRoomType;
    private JComboBox<Integer> cmbNumberOfRooms; // ComboBox for number of rooms
    private JComboBox<Integer> cmbCapacity;  
    private JCheckBox chckbxTv;
    private JCheckBox chckbxWifi;
    private JCheckBox chckbxRefrigerator;
    private JComboBox<String> comboBoxView;
    private JComboBox<String> comboBoxHotel;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                HotelRoomDefinition frame = new HotelRoomDefinition();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the frame.
     */
    public HotelRoomDefinition() {
        setResizable(false);
        setTitle("Hotel Room Definition"); 
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(160, 160, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null); // Set layout to null for absolute positioning
        setContentPane(contentPane);

        JLabel lblHotel = new JLabel("Hotel:");
        lblHotel.setBounds(10, 11, 80, 14);
        contentPane.add(lblHotel);

        comboBoxHotel = new JComboBox<>();
        comboBoxHotel.setBounds(127, 8, 150, 20);
        contentPane.add(comboBoxHotel);
        populateHotelsComboBox();
        
        JLabel lblRoomType = new JLabel("Room Type:");
        lblRoomType.setBounds(10, 36, 80, 14);
        contentPane.add(lblRoomType);

        txtRoomType = new JTextField();
        txtRoomType.setBounds(127, 33, 150, 20);
        contentPane.add(txtRoomType);
        txtRoomType.setColumns(10);

        JLabel lblNumberOfRooms = new JLabel("Number of Rooms:");
        lblNumberOfRooms.setBounds(10, 61, 120, 14);
        contentPane.add(lblNumberOfRooms);

        cmbNumberOfRooms = new JComboBox<>();
        for (int i = 1; i <= 200; i++) {
            cmbNumberOfRooms.addItem(i);
        }
        cmbNumberOfRooms.setBounds(127, 58, 86, 20);
        contentPane.add(cmbNumberOfRooms);

        JLabel lblCapacity = new JLabel("Capacity:");
        lblCapacity.setBounds(10, 86, 80, 14);
        contentPane.add(lblCapacity);

        cmbCapacity = new JComboBox<>();
        for (int i = 1; i <= 9; i++) {
            cmbCapacity.addItem(i);
        }
        cmbCapacity.setBounds(127, 86, 86, 20);
        contentPane.add(cmbCapacity);

        comboBoxView = new JComboBox<>();
        comboBoxView.setModel(new DefaultComboBoxModel<>(new String[] {"Sea View", "Land View"}));
        comboBoxView.setBounds(131, 111, 116, 22);
        contentPane.add(comboBoxView);

        JLabel lblView = new JLabel("View:");
        lblView.setBounds(14, 112, 80, 14);
        contentPane.add(lblView);

        chckbxTv = new JCheckBox("TV");
        chckbxTv.setBounds(10, 137, 97, 23);
        contentPane.add(chckbxTv);

        chckbxWifi = new JCheckBox("WiFi");
        chckbxWifi.setBounds(109, 137, 97, 23);
        contentPane.add(chckbxWifi);

        chckbxRefrigerator = new JCheckBox("Refrigerator");
        chckbxRefrigerator.setBounds(208, 137, 97, 23);
        contentPane.add(chckbxRefrigerator);

        JButton btnSaveRoom = new JButton("Save Room");
        btnSaveRoom.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveRoomData();
            }
        });
        btnSaveRoom.setBounds(10, 171, 117, 23);
        contentPane.add(btnSaveRoom);
    }

    private void populateHotelsComboBox() {
        String query = "SELECT hotels.name, cities.name FROM hotels JOIN cities ON hotels.city_id = cities.id";
        try (Connection conn = DatabaseUtil.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query);
             ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                String hotelName = rs.getString(1) + " - " + rs.getString(2);
                comboBoxHotel.addItem(hotelName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void saveRoomData() {
        String hotelComboItem = (String) comboBoxHotel.getSelectedItem();
        String[] hotelInfo = hotelComboItem.split(" - ");
        String hotelName = hotelInfo[0].trim();
        String city = hotelInfo.length > 1 ? hotelInfo[1].split(" \\(")[0].trim() : "";

        String roomType = txtRoomType.getText();
        int numberOfRooms = (Integer) cmbNumberOfRooms.getSelectedItem();
        int capacity = (Integer) cmbCapacity.getSelectedItem();

        // Get the state of TV, WiFi, and refrigerator checkboxes
        int tv = chckbxTv.isSelected() ? 1 : 0;
        int wifi = chckbxWifi.isSelected() ? 1 : 0;
        int refrigerator = chckbxRefrigerator.isSelected() ? 1 : 0;

        // Get the selected view
        String view = (String) comboBoxView.getSelectedItem();

        // Save hotel name, city name, and other details in separate columns
        String query = "INSERT INTO rooms (hotel_id, room_type, number_of_rooms, capacity, view, tv, wifi, refrigerator) "
                     + "VALUES ((SELECT id FROM hotels WHERE name = ?), ?, ?, ?, ?, ?, ?, ?)";

        try (Connection conn = DatabaseUtil.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setString(1, hotelName);
            stmt.setString(2, roomType);
            stmt.setInt(3, numberOfRooms);
            stmt.setInt(4, capacity);
            stmt.setString(5, view);
            stmt.setInt(6, tv);
            stmt.setInt(7, wifi);
            stmt.setInt(8, refrigerator);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(this, "Room data saved successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error saving room data.");
            ex.printStackTrace();
        }
    }
}
