package hotelReservation;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.sql.*;
import java.awt.Toolkit;

/**
 * Class ListReservation - Defines the GUI and functionality for listing reservations.
 * Author: Mohammad Inanloo
 */
public class ListReservation extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTable tableReservations;
    private DefaultTableModel tableModel;
    private JTextField searchField;
    private TableRowSorter<DefaultTableModel> sorter;

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                ListReservation frame = new ListReservation();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public ListReservation() {
        setResizable(false);
        setIconImage(Toolkit.getDefaultToolkit().getImage(ListReservation.class.getResource("/resources/calendar.png")));
        setTitle("List Reservation");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(180, 180, 927, 455); // Adjusted for search field
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        // Initialize table model and table
        tableModel = new DefaultTableModel(
            new String[] {
                "Passenger Name", "Hotel Name", "City", "Room Type", "View", "Check-In Date", "Nights"
            }, 0
        ) {
            private static final long serialVersionUID = 1L;

            @Override
            public Class<?> getColumnClass(int column) {
                return (column == 5) ? Date.class : String.class;
            }
        };

        tableReservations = new JTable(tableModel);
        sorter = new TableRowSorter<>(tableModel);
        tableReservations.setRowSorter(sorter);

        JScrollPane scrollPane = new JScrollPane(tableReservations);
        scrollPane.setBounds(10, 50, 891, 350);
        contentPane.add(scrollPane);

        // Search field
        searchField = new JTextField();
        searchField.setBounds(129, 14, 150, 25);
        contentPane.add(searchField);

        JButton searchButton = new JButton("Search");
        searchButton.setBounds(289, 14, 80, 25);
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = searchField.getText();
                if (text.trim().length() == 0) {
                    sorter.setRowFilter(null);
                } else {
                    sorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }
        });
        contentPane.add(searchButton);
        
        JLabel lblNewLabel = new JLabel("Passenger Name:");
        lblNewLabel.setBounds(10, 14, 109, 25);
        contentPane.add(lblNewLabel);

        loadReservations(); // Load reservation data into the table
    }

    private void loadReservations() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        String query = "SELECT reservations.passenger_name, hotels.name AS hotel_name, cities.name AS city_name, "
                     + "reservations.room_type, reservations.view, reservations.check_in_date, reservations.nights "
                     + "FROM reservations "
                     + "JOIN hotels ON reservations.hotel_id = hotels.id "
                     + "JOIN cities ON hotels.city_id = cities.id";
        try (Connection conn = DatabaseUtil.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query);
             ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                String passengerName = rs.getString("passenger_name");
                String hotelName = rs.getString("hotel_name");
                String cityName = rs.getString("city_name");
                String roomType = rs.getString("room_type");
                String view = rs.getString("view");
                Date checkInDate = rs.getDate("check_in_date");
                int nights = rs.getInt("nights");

                tableModel.addRow(new Object[]{passengerName, hotelName, cityName, roomType, view, checkInDate, nights});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, "Error reading reservations from database.", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
}
