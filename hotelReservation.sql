/*
 Navicat Premium Data Transfer

 Source Server         : mysql docker
 Source Server Type    : MySQL
 Source Server Version : 50744
 Source Host           : localhost:3306
 Source Schema         : hotelReservation

 Target Server Type    : MySQL
 Target Server Version : 50744
 File Encoding         : 65001

 Date: 19/05/2024 20:57:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cities
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cities
-- ----------------------------
INSERT INTO `cities` VALUES (1, 'New York');
INSERT INTO `cities` VALUES (2, 'Los Angeles');
INSERT INTO `cities` VALUES (3, 'Chicago');
INSERT INTO `cities` VALUES (4, 'Houston');
INSERT INTO `cities` VALUES (5, 'Phoenix');
INSERT INTO `cities` VALUES (6, 'Philadelphia');
INSERT INTO `cities` VALUES (7, 'San Antonio');
INSERT INTO `cities` VALUES (8, 'San Diego');
INSERT INTO `cities` VALUES (9, 'Dallas');
INSERT INTO `cities` VALUES (10, 'San Jose');
INSERT INTO `cities` VALUES (11, 'Austin');
INSERT INTO `cities` VALUES (12, 'Jacksonville');
INSERT INTO `cities` VALUES (13, 'Fort Worth');
INSERT INTO `cities` VALUES (14, 'Columbus');
INSERT INTO `cities` VALUES (15, 'Charlotte');
INSERT INTO `cities` VALUES (16, 'San Francisco');
INSERT INTO `cities` VALUES (17, 'Indianapolis');
INSERT INTO `cities` VALUES (18, 'Seattle');
INSERT INTO `cities` VALUES (19, 'Denver');
INSERT INTO `cities` VALUES (20, 'Washington');
INSERT INTO `cities` VALUES (21, 'Boston');
INSERT INTO `cities` VALUES (22, 'El Paso');
INSERT INTO `cities` VALUES (23, 'Nashville');
INSERT INTO `cities` VALUES (24, 'Detroit');
INSERT INTO `cities` VALUES (25, 'Oklahoma City');
INSERT INTO `cities` VALUES (26, 'Portland');
INSERT INTO `cities` VALUES (27, 'Las Vegas');
INSERT INTO `cities` VALUES (28, 'Memphis');
INSERT INTO `cities` VALUES (29, 'Louisville');
INSERT INTO `cities` VALUES (30, 'Baltimore');
INSERT INTO `cities` VALUES (31, 'Milwaukee');
INSERT INTO `cities` VALUES (32, 'Albuquerque');
INSERT INTO `cities` VALUES (33, 'Tucson');
INSERT INTO `cities` VALUES (34, 'Fresno');
INSERT INTO `cities` VALUES (35, 'Sacramento');
INSERT INTO `cities` VALUES (36, 'Kansas City');
INSERT INTO `cities` VALUES (37, 'Mesa');
INSERT INTO `cities` VALUES (38, 'Atlanta');
INSERT INTO `cities` VALUES (39, 'Omaha');
INSERT INTO `cities` VALUES (40, 'Colorado Springs');
INSERT INTO `cities` VALUES (41, 'Raleigh');
INSERT INTO `cities` VALUES (42, 'Miami');
INSERT INTO `cities` VALUES (43, 'Long Beach');
INSERT INTO `cities` VALUES (44, 'Virginia Beach');
INSERT INTO `cities` VALUES (45, 'Oakland');
INSERT INTO `cities` VALUES (46, 'Minneapolis');
INSERT INTO `cities` VALUES (47, 'Tulsa');
INSERT INTO `cities` VALUES (48, 'Arlington');
INSERT INTO `cities` VALUES (49, 'Tampa');

-- ----------------------------
-- Table structure for hotels
-- ----------------------------
DROP TABLE IF EXISTS `hotels`;
CREATE TABLE `hotels`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `stars` int(11) NOT NULL,
  `city_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `city_id`(`city_id`) USING BTREE,
  CONSTRAINT `hotels_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hotels
-- ----------------------------
INSERT INTO `hotels` VALUES (1, 'Hotel 1', 3, 11);
INSERT INTO `hotels` VALUES (2, 'Hotel 2', 4, 1);

-- ----------------------------
-- Table structure for reservations
-- ----------------------------
DROP TABLE IF EXISTS `reservations`;
CREATE TABLE `reservations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `passenger_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hotel_id` int(11) NULL DEFAULT NULL,
  `room_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `view` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `check_in_date` date NOT NULL,
  `nights` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `hotel_id`(`hotel_id`) USING BTREE,
  CONSTRAINT `reservations_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reservations
-- ----------------------------
INSERT INTO `reservations` VALUES (1, 'Mohammad', 1, 'ROOM LUX', 'Sea View', '2024-05-04', 5);
INSERT INTO `reservations` VALUES (2, 'akbar', 2, 'Seaview', 'Sea View', '2025-01-08', 6);

-- ----------------------------
-- Table structure for rooms
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NULL DEFAULT NULL,
  `room_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `number_of_rooms` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `view` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tv` tinyint(1) NOT NULL,
  `wifi` tinyint(1) NOT NULL,
  `refrigerator` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `hotel_id`(`hotel_id`) USING BTREE,
  CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rooms
-- ----------------------------
INSERT INTO `rooms` VALUES (1, 1, 'ROOM LUX', 15, 5, 'Sea View', 1, 0, 1);
INSERT INTO `rooms` VALUES (2, 1, 'ROOM SEA VIEW', 10, 5, 'Sea View', 0, 1, 1);
INSERT INTO `rooms` VALUES (3, 2, 'Seaview', 4, 1, 'Sea View', 0, 1, 1);

SET FOREIGN_KEY_CHECKS = 1;
